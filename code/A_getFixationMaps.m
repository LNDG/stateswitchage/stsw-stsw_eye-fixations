
IDs = {'1117';'1118';'1120';'1124';'1126';'1131';'1132';'1135';'1136';'1138';...
    '1144';'1151';'1158';'1160';'1163';'1164';'1167';'1169';'1172';'1173';...
    '1178';'1182';'1215';'1216';'1219';'1221';'1223';'1227';'1228';'1233';...
    '1234';'1237';'1239';'1240';'1243';'1245';'1247';'1250';'1252';'1257';...
    '1261';'1265';'1266';'1268';'1270';'1276';'1281'};

hist_mat = []; FDM5 = []; hist_mat_var = [];FDM5_var =[];
for indID = 1:numel(IDs)
    disp([IDs{indID}]);
    data = load(['/Volumes/LNDG/Projects/StateSwitch/dynamic/data/eye/SA_EEG/A_preprocessing/B_data/E_eye_FT_cleaned/',IDs{indID},'_eyeData.mat']);

    % calculate by condition to check for differences

    for indCond = 1:4
        TRL{indCond} = find(data.eyeData.TrlInfo.StateOrders(indID,:) == indCond);
    end

    tmp_hist_mat = []; tmp_FDM_5 = [];
    for indCond = 1:4
        for indTrial = 1:numel(TRL{indCond})
            timeIdx = find(data.eyeData.time{1}>0 & data.eyeData.time{1}<3);
            d = data.eyeData.trial{TRL{indCond}(indTrial)}(2:3,timeIdx);
            d(isnan(d)) = 0;
            d(:,nanmean(d,1)==0) = [];
            % bin size 10 x 10 pixel
            tmp_hist_mat(indTrial,:,:) = histcounts2(d(1,:),d(2,:),1:5:1919,1:5:1079);
            tmp_FDM_5(indTrial,:,:) = imgaussfilt(tmp_hist_mat(indTrial,:,:),5);    
        end
        hist_mat(indID,indCond,:,:) = squeeze(nanmean(tmp_hist_mat,1));
        FDM5(indID,indCond,:,:) = squeeze(nanmean(tmp_FDM_5,1));
        
        hist_mat_var(indID,indCond,:,:) = squeeze(nanstd(tmp_hist_mat,[],1));
        FDM5_var(indID,indCond,:,:) = squeeze(nanstd(tmp_FDM_5,[],1));
    end
end

save('/Volumes/LNDG/Projects/StateSwitch/dynamic/data/eye/SA_EEG/B_analyses/C_fixations/B_data/A_FDM_YA.mat', ...
    'FDM5*', 'hist_mat*')

load('/Volumes/LNDG/Projects/StateSwitch/dynamic/data/eye/SA_EEG/B_analyses/C_fixations/B_data/A_FDM_YA.mat', ...
    'FDM5', 'hist_mat')

FDM5 = permute(FDM5, [1,2,4,3]);
hist_mat = permute(hist_mat, [1,2,4,3]);
FDM5_var = permute(FDM5_var, [1,2,4,3]);
hist_mat_var = permute(hist_mat_var, [1,2,4,3]);

figure; 
subplot(1,2,1); imagesc(squeeze(nanmean(nanmean(hist_mat,2),1))); colormap('hot')
subplot(1,2,2); imagesc(squeeze(nanmean(nanmean(FDM5,2),1))); colormap('hot')

figure; 
subplot(1,2,1); imagesc(squeeze(nanmean(nanmean(hist_mat(:,2:4,:,:),2)-hist_mat(:,1,:,:),1)),[-.2 .2]); colormap('hot')
subplot(1,2,2); imagesc(squeeze(nanmean(nanmean(FDM5(:,2:4,:,:),2)-FDM5(:,1,:,:),1)),[-.2 .2]); colormap('hot')
colormap('parula')

load('/Volumes/LNDG/Projects/StateSwitch/dynamic/data/multimodal/C2_attentionFactor_OA/B_data/A_EEGAttentionFactor_YAOA.mat')

EEGAttentionFactor.PCAalphaGamma(37) = NaN;
EEGAttentionFactor.PCAalphaGamma(28) = NaN;
EEGAttentionFactor.PCAalphaGamma(69) = NaN;

attFctrIdx = find(ismember(EEGAttentionFactor.IDs,IDs));
IDIdx = find(ismember(IDs,EEGAttentionFactor.IDs(attFctrIdx)));

[~, sortIdx] = sort(EEGAttentionFactor.PCAalphaGamma(attFctrIdx), 'ascend');
lowMod = sortIdx(1:ceil(numel(sortIdx)/2));
highMod = sortIdx(ceil(numel(sortIdx)/2+1:end));

figure;
subplot(2,3,1); cla; imagesc(squeeze(nanmean(nanmean(FDM5(IDIdx(lowMod),1:4,:,:),2),1)),[-5 5]); colorbar;
    viscircles([floor(size(FDM5,4)/2), floor(size(FDM5,3)/2)],[floor(size(FDM5,4)/2)/4], 'Color', 'k')
    viscircles([floor(size(FDM5,4)/2), floor(size(FDM5,3)/2)],[2], 'Color', 'r')
    title('Avg. fixation low modulators')
subplot(2,3,2); imagesc(squeeze(nanmean(nanmean(FDM5(IDIdx(highMod),1:4,:,:),2),1)),[-5 5]); colorbar;
    viscircles([floor(size(FDM5,4)/2), floor(size(FDM5,3)/2)],[floor(size(FDM5,4)/2)/4], 'Color', 'k')
    viscircles([floor(size(FDM5,4)/2), floor(size(FDM5,3)/2)],[2], 'Color', 'r')
    title('Avg. fixation high modulators')
subplot(2,3,3); imagesc(squeeze(nanmean(nanmean(FDM5(IDIdx(lowMod),1:4,:,:),2),1))-squeeze(nanmean(nanmean(FDM5(IDIdx(highMod),1:4,:,:),2),1)),[-.5 .5]); colorbar;
    viscircles([floor(size(FDM5,4)/2), floor(size(FDM5,3)/2)],[floor(size(FDM5,4)/2)/4], 'Color', 'k')
    viscircles([floor(size(FDM5,4)/2), floor(size(FDM5,3)/2)],[2], 'Color', 'r')
    title('Avg. fixation low-high modulators')
subplot(2,3,4); imagesc(squeeze(nanmean(nanmean(FDM5(IDIdx(lowMod),2:4,:,:),2)-FDM5(IDIdx(lowMod),1,:,:),1)),[-.3 .3]); colorbar;
    viscircles([floor(size(FDM5,4)/2), floor(size(FDM5,3)/2)],[floor(size(FDM5,4)/2)/4], 'Color', 'k')
    viscircles([floor(size(FDM5,4)/2), floor(size(FDM5,3)/2)],[2], 'Color', 'r')
    title('Uncertainty modulation low modulators')
subplot(2,3,5); imagesc(squeeze(nanmean(nanmean(FDM5(IDIdx(highMod),2:4,:,:),2)-FDM5(IDIdx(highMod),1,:,:),1)),[-.3 .3]); colorbar;
    viscircles([floor(size(FDM5,4)/2), floor(size(FDM5,3)/2)],[floor(size(FDM5,4)/2)/4], 'Color', 'k')
    viscircles([floor(size(FDM5,4)/2), floor(size(FDM5,3)/2)],[2], 'Color', 'r')
    title('Uncertainty modulation high modulators')
subplot(2,3,6); imagesc(squeeze(nanmean(nanmean(FDM5_var(IDIdx(highMod),1:4,:,:),2),1)),[-1 1]); colorbar;
    viscircles([floor(size(FDM5_var,4)/2), floor(size(FDM5_var,3)/2)],[floor(size(FDM5_var,4)/2)/4], 'Color', 'k')
    viscircles([floor(size(FDM5_var,4)/2), floor(size(FDM5_var,3)/2)],[2], 'Color', 'r')
    title('Fixation variability')
colormap('parula')
set(findall(gcf,'-property','FontSize'),'FontSize',18)

figure;
imagesc((squeeze(nanmean(nanmean(FDM5(IDIdx(lowMod),2:4,:,:),2),1))-squeeze(nanmean(nanmean(FDM5(IDIdx(lowMod),1,:,:),2),1)))-...
    (squeeze(nanmean(nanmean(FDM5(IDIdx(highMod),2:4,:,:),2),1))-squeeze(nanmean(nanmean(FDM5(IDIdx(highMod),1,:,:),2),1))),[-.05 .05]); colorbar;
    viscircles([floor(size(FDM5,4)/2), floor(size(FDM5,3)/2)],[floor(size(FDM5,4)/2)/4], 'Color', 'k')
    viscircles([floor(size(FDM5,4)/2), floor(size(FDM5,3)/2)],[2], 'Color', 'r')
colormap('parula')
set(findall(gcf,'-property','FontSize'),'FontSize',18)

figure;
subplot(2,3,1); cla; imagesc(squeeze(nanmean(nanmean(FDM5_var(IDIdx(lowMod),1:4,:,:),2),1)),[-1 1]); colorbar;
    viscircles([floor(size(FDM5,4)/2), floor(size(FDM5,3)/2)],[floor(size(FDM5,4)/2)/4], 'Color', 'k')
    viscircles([floor(size(FDM5,4)/2), floor(size(FDM5,3)/2)],[2], 'Color', 'r')
    title('SD fixation low modulators')
subplot(2,3,2); imagesc(squeeze(nanmean(nanmean(FDM5_var(IDIdx(highMod),1:4,:,:),2),1)),[-1 1]); colorbar;
    viscircles([floor(size(FDM5,4)/2), floor(size(FDM5,3)/2)],[floor(size(FDM5,4)/2)/4], 'Color', 'k')
    viscircles([floor(size(FDM5,4)/2), floor(size(FDM5,3)/2)],[2], 'Color', 'r')
    title('SD fixation high modulators')
subplot(2,3,3); imagesc(squeeze(nanmean(nanmean(FDM5_var(IDIdx(lowMod),1:4,:,:),2),1))-squeeze(nanmean(nanmean(FDM5(IDIdx(highMod),1:4,:,:),2),1)),[-.05 .05]); colorbar;
    viscircles([floor(size(FDM5,4)/2), floor(size(FDM5,3)/2)],[floor(size(FDM5,4)/2)/4], 'Color', 'k')
    viscircles([floor(size(FDM5,4)/2), floor(size(FDM5,3)/2)],[2], 'Color', 'r')
    title('SD fixation low-high modulators')
subplot(2,3,4); imagesc(squeeze(nanmean(nanmean(FDM5_var(IDIdx(lowMod),2:4,:,:),2)-FDM5_var(IDIdx(lowMod),1,:,:),1)),[-.3 .3]); colorbar;
    viscircles([floor(size(FDM5,4)/2), floor(size(FDM5,3)/2)],[floor(size(FDM5,4)/2)/4], 'Color', 'k')
    viscircles([floor(size(FDM5,4)/2), floor(size(FDM5,3)/2)],[2], 'Color', 'r')
    title('Uncertainty modulation SD low modulators')
subplot(2,3,5); imagesc(squeeze(nanmean(nanmean(FDM5_var(IDIdx(highMod),2:4,:,:),2)-FDM5_var(IDIdx(highMod),1,:,:),1)),[-.3 .3]); colorbar;
    viscircles([floor(size(FDM5,4)/2), floor(size(FDM5,3)/2)],[floor(size(FDM5,4)/2)/4], 'Color', 'k')
    viscircles([floor(size(FDM5,4)/2), floor(size(FDM5,3)/2)],[2], 'Color', 'r')
    title('Uncertainty modulation SD high modulators')
colormap('parula')
set(findall(gcf,'-property','FontSize'),'FontSize',18)

figure;
for indID = 1:numel(IDs)
    cla; imagesc(squeeze(nanmean(nanmean(FDM5_var(indID,1:4,:,:),2),1)),[-1 1]); colorbar;
    viscircles([floor(size(FDM5,4)/2), floor(size(FDM5,3)/2)],[floor(size(FDM5,4)/2)/4], 'Color', 'k')
    viscircles([floor(size(FDM5,4)/2), floor(size(FDM5,3)/2)],[2], 'Color', 'r')
    title('Individual SD fixation')
    pause(.5)
end

1080/265 % 4.05
1024/265 % 3.85

figure;
subplot(2,3,1); imagesc(squeeze(nanmean(nanmean(FDM5(IDIdx(lowMod),1:4,:,:),2),1)),[-5 5]); 
    xlim([floor(size(FDM5,4)/3), size(FDM5,4)-floor(size(FDM5,4)/3)]); ylim([floor(size(FDM5,3)/3), size(FDM5,3)-floor(size(FDM5,3)/3)]); colorbar;
subplot(2,3,2); imagesc(squeeze(nanmean(nanmean(FDM5(IDIdx(highMod),1:4,:,:),2),1)),[-5 5]);
    xlim([floor(size(FDM5,4)/3), size(FDM5,4)-floor(size(FDM5,4)/3)]); ylim([floor(size(FDM5,3)/3), size(FDM5,3)-floor(size(FDM5,3)/3)]); colorbar;
subplot(2,3,3); imagesc(squeeze(nanmean(nanmean(FDM5(IDIdx(lowMod),1:4,:,:),2),1))-squeeze(nanmean(nanmean(FDM5(IDIdx(highMod),1:4,:,:),2),1)),[-.5 .5]); 
    xlim([floor(size(FDM5,4)/3), size(FDM5,4)-floor(size(FDM5,4)/3)]); ylim([floor(size(FDM5,3)/3), size(FDM5,3)-floor(size(FDM5,3)/3)]); colorbar;
subplot(2,3,4); imagesc(squeeze(nanmean(nanmean(FDM5(IDIdx(lowMod),2:4,:,:),2)-FDM5(IDIdx(lowMod),1,:,:),1)),[-.3 .3]);
    xlim([floor(size(FDM5,4)/3), size(FDM5,4)-floor(size(FDM5,4)/3)]); ylim([floor(size(FDM5,3)/3), size(FDM5,3)-floor(size(FDM5,3)/3)]); colorbar;
subplot(2,3,5); imagesc(squeeze(nanmean(nanmean(FDM5(IDIdx(highMod),2:4,:,:),2)-FDM5(IDIdx(highMod),1,:,:),1)),[-.3 .3]);
    xlim([floor(size(FDM5,4)/3), size(FDM5,4)-floor(size(FDM5,4)/3)]); ylim([floor(size(FDM5,3)/3), size(FDM5,3)-floor(size(FDM5,3)/3)]); colorbar;
colormap('parula')

% next steps: significance test of load effect
% comparison of low and high modulators

%% archival code for testing different tools

%addpath(genpath('/Volumes/LNDG/Projects/StateSwitch/dynamic/data/eye/SA_EEG/A_preprocessing/D_tools/EyeMMV-master'))

%fixation_detection('data_stimulus_demo.txt',0.250,0.100,150,1.25,1.00);

% data = load('/Volumes/LNDG/Projects/StateSwitch/dynamic/data/eye/SA_EEG/A_preprocessing/B_data/E_eye_FT_cleaned/1120_eyeData.mat')

% indTrial = 1
% [fixation_list_t2,fixation_list_3s]=fixation_detection([data.eyeData.trial{1}(2:3,:); data.eyeData.trial{1}(1,:)]', 0.250,0.100,200,1919,1079);
% [fixation_list_t2,fixation_list_3s]=fixation_detection([data.eyeData.trial{1}(2:3,:); data.eyeData.trial{1}(1,:)]', 0.250,0.100,150,1919,1079);
% 
% addpath(genpath('/Users/kosciessa/Downloads/Auto_Heat_Mapper-master'))
% 
% d = data.eyeData.trial{indTrial}(2:3,:);
% d(isnan(d)) = 0;
% d(:,nanmean(d,1)==0) = [];
% p = []; p = gkde2(d);
% 
% figure
% surf(p.x./1919,p.y./1079,p.pdf*1000000,'FaceAlpha','interp',...
%     'AlphaDataMapping','scaled',...
%     'AlphaData',p.pdf*1000000,...
%     'FaceColor','interp','EdgeAlpha',0);
% view(2)
% xlim([0 1]); ylim([0 1])


% figure; scatter(fixation_list_t2(:,1), fixation_list_t2(:,2), 'filled');
% 
% fixation_detection(edf.data(:,1:3), 0.250,0.100,1919,1079,1.00);
% 
% data = load('/Volumes/LNDG/Projects/StateSwitch/dynamic/data/eye/SA_EEG/A_preprocessing/B_data/E_eye_FT_cleaned/1117_eyeData.mat')
% 
% figure; plot(data.eyeData.trial{1}(2,:))
% figure; plot(data.eyeData.trial{1}(3,:))
% 
% edf = load('/Volumes/LNDG/Projects/StateSwitch/dynamic/data/eye/SA_EEG/A_preprocessing/B_data/C_eye_mat/S1117r1.mat')
% 
% figure; plot(edf.data(:,2))
% figure; plot(edf.data(:,3))
% 
% figure; imagesc(data.eyeData.trial{1}(2:end,:))
% 
% % data with accurate X & Y, segmented
% 
% load('/Volumes/LNDG/Projects/StateSwitch/dynamic/data/eeg/task/A_preproc/SA_preproc_study/B_data/C_EEG_FT/1117_r1_dynamic_eyeEEG_Rlm_Fhl_rdSeg.mat')
% figure; imagesc(data.trial{1}(67:68,:))
