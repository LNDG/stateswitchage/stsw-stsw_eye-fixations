% get an estimate of the radius of the circle

% Change x,y coordinates to pixels (y is inverted - pos on bottom, neg. on top)
center = [center(:,1) + expInfo.apXYD(:,1)/10*screenInfo.ppd center(:,2) - ...
    expInfo.apXYD(:,2)/10*screenInfo.ppd]; % where you want the center of the aperture
center(:,3) = expInfo.apXYD(:,3)/2/10*screenInfo.ppd; % add diameter
d_ppd = floor(apD/10 * screenInfo.ppd);	% size of aperture in pixels
dotSize = expInfo.dotSize; % probably better to leave this in pixels, but not sure

(130/2/10)*ppd

130/10 * screenInfo.ppd

width = 50;
distance = 50;

% Determine pixels per degree (ppd), which unit is derived as
% (pix/screen) * (screen/rad) * (rad/deg) = (pixels per degree)
screenInfo.ppd = pi * screenInfo.screenRect(3) / atan(monWidth/viewDist/2) / 360;    

% 1080*1024

ppd = pi*1080/atan(50/50/2)/360;

radiusInPixels = 130/10 * ppd;

% 265 pixels

1080/265 % 4.05
1024/265 % 3.85